# Prerrequisitos

* SO ubuntu/debian
* Make 
  * ``` $ sudo apt update && apt install make ```
* [Docker](https://docs.docker.com/engine/install/ubuntu/) y [Docker-compose](https://docs.docker.com/compose/install/)
* Disponer de un Cluster de kubernetes, Minikube, microk8s, kubeadm o en cloud, mas su archivo de kubeconfig para acceder a él.
* [kubectl](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/) 
* [Helm](https://helm.sh/docs/intro/install/#from-apt-debianubuntu)
* [Terraform](https://www.terraform.io/downloads.html)
* Openssl
  * ``` $ sudo apt update && apt install openssl ```

Para CI y CD 

* Gitlab

# Ejecutar Retos

Para ejecutar los retos se debe realizar mediante comandos make.

## Reto 1. Dockerize la aplicación

1. **Construir imagen:**

``` $ make build-image ```

2. **Crear contenedor:**

``` $ make run-container ```

3. **Probar contenedor:**

```bash
 $ curl -s localhost:3000/ | jq
 {
   "msg": "ApiRest prueba"
 }
```
## Reto 2. Docker Compose

1. **Crear certificados ssl self signed, en el proceso llenar el campo: server FQDN or YOUR name con el dominio a utilizar, en el proyecto ya existen certificados por defecto para: reto-devops.example**

``` $ make create-ssl ```

2. *(opcional) modificar tabla de hosts para utilizar el dominio seleccionado* 

``` 
$ sudo vi /etc/hosts

127.0.0.1  reto-devops.example
```

3. **Crear credenciales para basic auth, en el proyecto ya existe un .htpasswd con usuario admin y password admin, al ejecutar el comando se sobrescribe**

``` $ make create-user USER=<username> ```

4. **Ejecutar docker compose**

``` $ make run-docker-compose ```

5. **Probar aplicación y seguridad**

domain.example = localhost o dominio configurado en /etc/hosts

```bash
$ curl -ks https://domain.example/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -ks https://domain.example/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -u admin:admin -ks https://domain.example/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

6. **Detener docker compose**

``` $ make stop-docker-compose ```


## Reto 3. Probar la aplicación en cualquier sistema CI/CD

Para el proceso de CI/CD se utiliza gitlab-ci, realizando los stage build test y release:
 
* build: comprueba que la construcción de la imagen se realice de manera exitosa.
* test: comprueba que los test unitarios de la aplicación estén ok.
* release: construye la imagen y la sube al registry de gitlab.

## Reto 4. Deploy en kubernetes

Para desplegar la aplicación a kubernetes se utilizara kubectl por lo que debe estar configurado para acceder al cluster de kubernetes.

Se desplegará por defecto en el namespace default, si desea modificarlo realizarlo en el archivo yaml en devops/kubernetes/deployment-reto-devops.yaml

La imagen docker utilizada es una imagen publica subida a dockerhub.

``` $ make deploy-app-kubernetes ```

## Reto 5. Construir Chart en helm y manejar trafico http(s)

Para el Helm Chart se utilizaran certificados ssl self signed para el dominio reto-devops.example, y para /private el usuario y password es admin:admin, por defecto sera desplegado en el namespace default.

Es necesario tener instalado helm y estar conectado al cluster y contexto para esta prueba.

los puertos utilizados para el reverse-proxy son el 30081 para http y 30082 para https

1. **Ejecutar helm** 

``` $ make helm-install ```

2. *(opcional) modificar tabla de hosts para utilizar el dominio seleccionado* 

``` 
$ sudo vi /etc/hosts

[ip-de-kubernetes]  reto-devops.example
```

3. **Probar nginx-rever-proxy**

domain.example = IP del nodo o reto-devops.example si se configuró en tabla de hosts

```bash
$ curl -ks https://domain.example:30082/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -ks https://domain.example:30082/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -u admin:admin -ks https://domain.example:30082/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

5. **Eliminar helm** 

``` $ make helm-delete ```
## Reto 6. Terraform

**Ejecutar Terraform**

Este comando make ejecutara, terraform init, terraform plan y terraform apply.
namespace por defecto: default

Al ejecutar el comando make se debe llenar dos valores, uno con el nombre del contexto de cluster de kubernetes y el otro la ruta del archivo kubeconfig del cluster ej: ~/.kube/config

Es posible crear un archivo terraform.tfvars en la ruta devops/terraform con los valores 

ej:
kube-config-path="~/.kube/config"
kube-config-context="contexto"


``` $ make terraform-apply ```