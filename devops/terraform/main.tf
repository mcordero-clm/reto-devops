terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
}

provider "kubernetes" {
  config_path    = var.kube-config-path
  config_context = var.kube-config-context
}

resource "kubernetes_role" "devops-reto-role" {
  metadata {
    name = "devops-reto-role"
    namespace = var.namespace
  }

  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "devops-reto-role-binding" {
  metadata {
    name      = "devops-reto-role-binding"
    namespace = var.namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.devops-reto-role.metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = var.namespace
  }
}