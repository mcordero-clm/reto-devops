variable "namespace" {
  description = "nombre del namespace de la aplicacion devops-reto"
  default = "default"
}

variable "kube-config-path" {
  description = "ruta de tu archivo kubeconfig"
}

variable "kube-config-context" {
  description = "nombre del contexto de kubeconfig"
}