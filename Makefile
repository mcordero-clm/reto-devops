build-image:
	docker build -t reto-devops:latest .

run-container:
	docker run -d -p 3000:3000 reto-devops:latest

create-ssl:
	sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout devops/docker-compose/certs/nginx.key -out devops/docker-compose/certs/nginx.crt

create-user:
	sudo htpasswd -c devops/docker-compose/basic-auth-passwd/.htpasswd ${USER}

run-docker-compose:
	docker-compose up -d

stop-docker-compose:
	docker-compose down

deploy-app-kubernetes:
	kubectl apply -f devops/kubernetes/deployment-reto-devops.yaml

helm-install:
	helm install nginx-reverse-proxy ./devops/kubernetes/nginx-reverse-proxy-helm -f ./devops/kubernetes/nginx-reverse-proxy-helm/values.yaml

helm-delete:
	helm delete nginx-reverse-proxy

terraform-init:
	cd devops/terraform/ && terraform init 

terraform-plan:
	cd devops/terraform/ && terraform plan

terraform-apply: terraform-init terraform-plan
	cd devops/terraform/ && terraform apply -auto-approve